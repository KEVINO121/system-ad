import { Request, Response } from "express";
import { User } from "../entity/user.entity";
import bcryptjs from "bcryptjs";
import { myDataSource } from "../../config";
import { sign, verify } from "jsonwebtoken";

export const Register = async (req: Request, res: Response) => {
  const { password, password_confirm, ...body } = req.body;

  if (password !== password_confirm) {
    return res.status(400).send({
      message: "the password is not the same",
    });
  }

  const user = await myDataSource.getRepository(User).save({
    ...body,
    password: await bcryptjs.hash(password, 10),
    is_ambassador: false,
  });
  delete user.password;
  res.send(user);
};

export const Login = async (req: Request, res: Response) => {
  const user = await myDataSource.getRepository(User).findOne({
    where: { email: req.body.email },
    select: ["id", "password"],
  });

  if (!user) {
    return res.status(400).send({
      message: "invalid credential",
    });
  }

  if (!(await bcryptjs.compare(req.body.password, user.password))) {
    return res.status(400).send({
      message: "password is incorrect",
    });
  }
  const token = sign(
    {
      id: user.id,
    },
    process.env.SECRET_KEY
  );

  res.cookie("jwt", token, {
    httpOnly: true,
    maxAge: 24 * 60 * 60 * 1000,
  });

  res.send({
    message: "succes",
  });

  //   res.send(token);
  //   res.send(user);
};

export const Authenticateduser = async (req: Request, res: Response) => {
  res.send(req["user"]);
};

export const logout = async (req: Request, res: Response) => {
  res.cookie("jwt", "", { maxAge: 0 });

  res.send({
    message: "succes",
  });
};

export const updateInfo = async (req: Request, res: Response) => {
  const user = req["user"];
  const repository = myDataSource.getRepository(User);

  await repository.update(user.id, req.body);

  res.send(
    await repository.findOne({
      where: user.id,
    })
  );
};

export const UpdatePassword = async (req: Request, res: Response) => {
  const user = req["user"];

  if (req.body.password !== req.body.password_confirm) {
    return res.status(400).send({
      message: "the password is not the same",
    });
  }

  await myDataSource.getRepository(User).update(user.id, {
    password: await bcryptjs.hash(req.body.password, 10),
  });
  res.send(user);
};
