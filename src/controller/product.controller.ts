import { Request, Response } from "express";
import { myDataSource } from "../../config";
import { Product } from "../entity/product.entity";

export const Products = async (req: Request, res: Response) => {
  res.send(await myDataSource.getRepository(Product).find());
};

export const CreateProduct = async (req: Request, res: Response) => {
  res
    .status(201)
    .send(await myDataSource.getRepository(Product).save(req.body));
};

export const GetProduct = async (req: Request, res: Response) => {
  res.send(
    await myDataSource
      .getRepository(Product)
      .findOne({ where: { id: parseInt(req.params.id, 10) } })
  );
};

export const UpdateProduct = async (req: Request, res: Response) => {
  const repository = myDataSource.getRepository(Product);

  await repository.update(req.params.id, req.body);
  res
    .status(204)
    .send(
      await repository.find({ where: { id: parseInt(req.params.id, 10) } })
    );
};

export const DeleteProduct = async (req: Request, res: Response) => {
  await myDataSource.getRepository(Product).delete(req.params.id);
  res.status(204).send(null);
};
