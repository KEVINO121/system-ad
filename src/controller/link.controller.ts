import { Request, Response } from "express";
import { myDataSource } from "../../config";
import { Link } from "../entity/link.entity";

export const Links = async (req: Request, res: Response) => {
  console.log(req.params.id);

  const links = await myDataSource.getRepository(Link).find({
    where: {
      user: req.params,
    },
  });
  res.send(links);
};
