import { Request, Response } from "express";
import { myDataSource } from "../../config";
import { User } from "../entity/user.entity";

export const Ambassador = async (req: Request, res: Response) => {
  res.send(
    await myDataSource.getRepository(User).find({
      where: {
        is_ambassador: true,
      },
    })
  );
};
