import express, { Request, Response } from "express";
import cors from "cors";

import { routes } from "./routes";
import { myDataSource } from "../config";
import dotenv from "dotenv";
import cookieParser from "cookie-parser";

dotenv.config();
myDataSource
  .initialize()
  .then(() => {
    const app = express();

    app.use(cookieParser());
    app.use(express.json());
    app.use(
      cors({
        origin: ["http://localhost:4200"],
      })
    );

    routes(app);

    app.listen(8000, () => {
      console.log("listening to port 8000");
    });
  })
  .catch((err) => {
    console.log("Error during data source", err);
  });
