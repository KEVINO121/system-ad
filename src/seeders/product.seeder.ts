import { myDataSource } from "../../config";
import { faker } from "@faker-js/faker";
import { Product } from "../entity/product.entity";
import { randomInt } from "crypto";

myDataSource.initialize().then(async () => {
  const repository = myDataSource.getRepository(Product);

  for (let i = 0; i < 30; i++) {
    await repository.save({
      title: faker.lorem.words(2),
      description: faker.lorem.words(10),
      image: faker.image.imageUrl(200, 200, "", true),
      price: faker.random.alphaNumeric(10),
    });
  }

  process.exit();
});
