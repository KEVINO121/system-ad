import express, { Router } from "express";
import {
  Authenticateduser,
  Login,
  logout,
  Register,
  updateInfo,
  UpdatePassword,
} from "./controller/auth.controller";
import { Links } from "./controller/link.controller";
import {
  Products,
  CreateProduct,
  GetProduct,
  UpdateProduct,
  DeleteProduct,
} from "./controller/product.controller";
import { Ambassador } from "./controller/user.controller";
import { AuthMiddleware } from "./middleware/auth.middleware";

export const routes = (router: Router) => {
  router.post("/api/admin/register", Register);
  router.post("/api/admin/login", Login);
  router.get("/api/admin/user", AuthMiddleware, Authenticateduser);
  router.post("/api/admin/logout", AuthMiddleware, logout);
  router.put("/api/admin/users/info", AuthMiddleware, updateInfo);
  router.put("/api/admin/users/password", AuthMiddleware, UpdatePassword);
  router.get("/api/admin/ambassador", AuthMiddleware, Ambassador);
  router.get("/api/admin/products", AuthMiddleware, Products);
  router.post("/api/admin/products", AuthMiddleware, CreateProduct);
  router.get("/api/admin/products/:id", AuthMiddleware, GetProduct);
  router.put("/api/admin/products/:id", AuthMiddleware, UpdateProduct);
  router.delete("/api/admin/products/:id", AuthMiddleware, DeleteProduct);
  router.get('/api/admin/users/:id/links', AuthMiddleware, Links)
};
